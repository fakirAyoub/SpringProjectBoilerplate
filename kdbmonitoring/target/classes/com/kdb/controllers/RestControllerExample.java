import org.springframework.web.bind.annotation.RestController;



//As the main purpose of a controller is to "orchestrate" the roles between the views and the action methods (inside services)
@RestController
public class RestControllerExample {
	
	@RequestMapping("/myFirstExample")
    public Greeting greeting(@RequestParam(value="name", defaultValue="Olivier") String name) {
        return new DatabaseDefault(4,
                            String.format(template, name));
    }
	
	/*If we go to localhost:8080/myFirstExample/, we will have "Olivier" that will be 
	returned as a json object since the default value is Olivier (that we've set in the parameter)
	otherwise if I go to localhost:8080/myFirstExample/Ayoub, the value that will be displayed
	will be "Ayoub", as a Json Object */
	
	
	//Seems to be simple and it is, huge projects have been made this way!
}
